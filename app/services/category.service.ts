import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';

import { ICategory } from "../models/category";


@Injectable()
export class CategoryService {
    // private _productUrl = 'api/products/products.json';

    constructor(private _http: Http) { }

    getCategories(): ICategory[] {
        return [
            { id: 1, name: "Food"},
            { id: 2, name: "Clothes"}
        ];
    }

    getCategory(id: number): ICategory {
        return this.getCategories().find((x) => x.id === id);
    }
}
