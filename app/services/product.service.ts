import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { IProduct } from "../models/product";


@Injectable()
export class ProductService {
    private products: IProduct[] = [
        { id: 1, name: 'Milk', price: 13, category: {id: 1, name: "Food"}, categoryId: 1},
        { id: 2, name: 'Bred', price: 6, category: {id: 1, name: "Food"}, categoryId: 1 },
        { id: 3, name: 'Jacket', price: 66, category: {id: 2, name: "Clothes"}, categoryId: 2 }
    ];

    constructor(private _http: Http) { }

    getProducts(): IProduct[] {
        return this.products;
    }

    getProduct(id: number): IProduct {
        return this.getProducts().find((x) => x.id === id);
    }

    addProduct(product: IProduct) {
        this.products.push(product);
    }

    nextId() {
        return Math.max(...this.products.map((x) => x.id)) + 1;
    }
}
