import { Component } from '@angular/core';
import { Router, OnActivate, RouteSegment, ROUTER_DIRECTIVES  }  from '@angular/router';

import { ProductService } from "../services/product.service";
import { CategoryService } from "../services/category.service";
import { IProduct } from "../models/Product";
import { ICategory } from "../models/Category";

@Component({
    templateUrl: 'app/products/product-edit.component.html',
    directives: [ROUTER_DIRECTIVES]
})
export class ProductEditComponent implements OnActivate {
    product: IProduct;
    categories: ICategory[];
    constructor(
        private router: Router, 
        private productService: ProductService,
        private categoryService: CategoryService
        ) {
        this.categories = categoryService.getCategories();
    }

    routerOnActivate(curr: RouteSegment): void {
        let id = +curr.getParam('id');
        if(id > 0)
            this.product = this.productService.getProduct(id);
        else 
            this.product = { id: -1, name: "", price: 0, category: this.categories[0], categoryId: this.categories[0].id};
    }

    backToList() {
        this.router.navigate(['/products']);
    }

    submit() {
        // alert(JSON.stringify(this.product))
        if(this.product.id <= 0)
            this.product.id = this.productService.nextId();
        this.product.categoryId = Number.parseInt(this.product.categoryId);
        this.product.category = this.categoryService.getCategory(this.product.categoryId);
        this.productService.addProduct(this.product);

        alert("product was saved " + JSON.stringify(this.product));
    }
}
