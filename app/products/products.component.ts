import { Component } from '@angular/core';
import { Router, ROUTER_DIRECTIVES } from '@angular/router';

import { IProduct } from "../models/product";
import { ProductService } from "../services/product.service";

@Component({
    templateUrl: 'app/products/products.component.html',
    directives: [ROUTER_DIRECTIVES]
})
export class ProductsComponent {
  tableHeader: string = "Products";

  constructor(
    private router: Router, 
    private productService: ProductService) {

  }

  getProducts(): IProduct[] {
      return this.productService.getProducts();
  }

  showDetails(product){
      this.router.navigate(['/productdetails', product.id]);
  }

  newProduct(){
      this.router.navigate(['/productedit', 0]);
  }
}
