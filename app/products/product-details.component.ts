import { Component } from '@angular/core';
import { Router, OnActivate, RouteSegment  }  from '@angular/router';
import { ROUTER_DIRECTIVES } from '@angular/router';

import { ProductService } from "../services/product.service";
import { IProduct } from "../models/Product";

@Component({
    templateUrl: 'app/products/product-details.component.html',
    directives: [ROUTER_DIRECTIVES]
})
export class ProductDetailsComponent implements OnActivate {
    product: IProduct;
    constructor(private router: Router, 
        private productService: ProductService) {
    }

    routerOnActivate(curr: RouteSegment): void {
        let id = +curr.getParam('id');
        this.product = this.productService.getProduct(id);
    }

    backToList() {
        this.router.navigate(['/products']);
    }
}
