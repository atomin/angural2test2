import { Component } from '@angular/core';
import { Router, OnActivate, RouteSegment  }  from '@angular/router';
import { ROUTER_DIRECTIVES } from '@angular/router';

import { CategoryService } from "../services/category.service";
import { ICategory } from "../models/category";

@Component({
    templateUrl: 'app/categories/category-details.component.html',
    directives: [ROUTER_DIRECTIVES]
})
export class CategoryDetailsComponent implements OnActivate {
    category: ICategory;
    constructor(private router: Router, 
        private categoryService: CategoryService) {
    }

    routerOnActivate(curr: RouteSegment): void {
        let id = +curr.getParam('id');
        this.category = this.categoryService.getCategory(id);
    }

    backToList() {
        this.router.navigate(['/categories']);
    }
}
