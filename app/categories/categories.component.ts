import { Component } from '@angular/core';
import { Router, ROUTER_DIRECTIVES } from '@angular/router';

import { ICategory } from "../models/category";
import { CategoryService } from "../services/category.service";

@Component({
    templateUrl: 'app/categories/categories.component.html',
    directives: [ROUTER_DIRECTIVES]
})
export class CategoriesComponent {
  tableHeader: string = "Categories";

  constructor(
    private router: Router, 
    private categoryService: CategoryService) {
  }

  getCategories(): ICategory[] {
      return this.categoryService.getCategories();
  }

  showDetails(category){
      this.router.navigate(['/categorydetails', category.id]);
  }
}
