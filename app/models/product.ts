import { ICategory } from "../models/Category";

export class IProduct {
    id: number;
    name: string;
    price: number;
    category: ICategory;
    categoryId: number;
}