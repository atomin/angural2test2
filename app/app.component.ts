import { Component } from '@angular/core';
import { HTTP_PROVIDERS } from '@angular/http';
import { ROUTER_PROVIDERS, Routes, ROUTER_DIRECTIVES } from '@angular/router';

import { ProductsComponent } from './products/products.component';
import { ProductEditComponent } from './products/product-edit.component';
import { ProductDetailsComponent } from './products/product-details.component';
import { ProductService } from "./services/product.service";
import { CategoryService } from "./services/category.service";
import { CategoriesComponent } from "./categories/categories.component";
import { CategoryDetailsComponent } from "./categories/category-details.component";

@Component({
  selector: 'my-app',
  templateUrl: 'app/app.component.html',
  directives: [ROUTER_DIRECTIVES],
  providers: [
      HTTP_PROVIDERS,
      ROUTER_PROVIDERS,
      ProductService,
      CategoryService
  ]
})
@Routes([
  { path: '/', component: ProductsComponent },
  { path: '/products', component: ProductsComponent },
  { path: '/productdetails/:id', component: ProductDetailsComponent },
  { path: '/productedit/:id', component: ProductEditComponent },
  { path: '/categories', component: CategoriesComponent },
  { path: '/categorydetails/:id', component: CategoryDetailsComponent }
])
export class AppComponent {
    pageTitle: string = 'Test shop catalog';
}
